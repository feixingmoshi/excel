# excel

#### 介绍
该插件是基于POI写的Excel导入导出模板插件，立志于只是用简单的注解配置，和简单的代码便能导出自己想要的Excel，降低Excel导出实体的依赖。

#### 软件架构
软件基于POI、自定义注解、java反射来实现的Excel导入导出的模板插件。


#### 安装教程

1. clone 本产品
2. 执行 mvn install:install-file -DgroupId=com.lwp -DartifactId=excel -Dversion=0.0.1-SNAPSHOT -Dpackaging=jar -Dfile=lwp-excel-0.0.1-SNAPSHOT.jar 讲项目部署到本地仓库 。
3. 项目中引入maven依赖 
```
        <dependency>
            <groupId>com.lwp</groupId>
            <artifactId>excel</artifactId>
            <version>0.0.1-SNAPSHOT</version>
        </dependency>
```


#### 使用说明

使用说明在我的博客，点击查看 [https://blog.csdn.net/qq_36622496](https://blog.csdn.net/qq_36622496)

#### 参与贡献

1. Fork 本仓库
2. 新建 Feat_xxx 分支
3. 提交代码（提交代码时，标注修改了哪些。）
4. 新建 Pull Request


