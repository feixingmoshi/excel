package com.lwp.excel.style;

public interface HalignStyles {
    short ALIGN_GENERAL = 0;//对齐一般
    short ALIGN_LEFT = 1;   //左对齐
    short ALIGN_CENTER = 2; //居中对齐
    short ALIGN_RIGHT = 3;  //右对齐
    short ALIGN_FILL = 4;   //对齐填补
    short ALIGN_JUSTIFY = 5;//两端对齐
    short ALIGN_CENTER_SELECTION = 6;//居中对齐选择
}
