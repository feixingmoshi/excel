package com.lwp.excel.test.test02VO;

import com.alibaba.fastjson.JSON;
import com.lwp.excel.util.ExcelUtil;
import org.apache.poi.hssf.usermodel.HSSFWorkbook;

import java.util.List;

/**
 * Copyright (C) @2019 GuangDong Eshore Technology Co. Ltd
 *
 * @author: Administrator
 * @version: 1.0
 * @date: 2019/8/27
 * @time: 15:22
 * @description:
 */
public class Test02 {


    public static void main(String[] args) {
        String jsonStr = "[{\"id\":\"1\",\"berthingFrequency\":100,\"remark\":\"无\",\"shipId\":\"1\",\"shipName\":\"和谐号\",\"totalVoyage\":100,\"createTime\":1566883349000,\"hostRunningTime\":{\"hostRunningTime1\":\"5\",\"hostRunningTime2\":\"4\",\"hostRunningTime3\":\"3\",\"hostRunningTime4\":\"2\"},\"inventory\":{\"oils0\":\"9\",\"oils10\":\"10\",\"summation\":\"19\"},\"lastMonthInventory\":{\"oils0\":\"1\",\"oils10\":\"2\",\"summation\":\"3\"},\"thisMonthPersonal\":{\"oils0\":\"7\",\"oils10\":\"8\",\"summation\":\"15\"},\"thisMonthReplenishment\":{\"oils0\":\"3\",\"oils10\":\"4\",\"summation\":\"7\"},\"thisMonthSupply\":{\"oils0\":\"5\",\"oils10\":\"6\",\"summation\":\"11\"}}]";

        List<MonthlyFuelConsumption> monthlyFuelConsumptions = JSON.parseArray(jsonStr, MonthlyFuelConsumption.class);
        String [] handers = new String[]{"shipName","lastMonthInventory","thisMonthReplenishment",
                "thisMonthSupply","thisMonthPersonal","inventory","hostRunningTime","totalVoyage",
                "berthingFrequency","remark","lastMonthInventory.oils0","lastMonthInventory.oils10",
                "thisMonthReplenishment.oils10","thisMonthReplenishment.summation","thisMonthSupply.oils0",
                "thisMonthPersonal.oils0","thisMonthPersonal.oils10","thisMonthPersonal.summation",
                "inventory.oils0","inventory.oils10","inventory.summation","hostRunningTime.hostRunningTime1","hostRunningTime.hostRunningTime2",
                "hostRunningTime.hostRunningTime3"};
        HSSFWorkbook wb = ExcelUtil.exportExcel(monthlyFuelConsumptions,handers);//,
        ExcelUtil.createExcelFile(wb,"/monthlyFuelConsumptions.xls");
        //System.out.println(monthlyFuelConsumptions);

    }

}
