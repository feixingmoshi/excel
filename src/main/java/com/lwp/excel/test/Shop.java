package com.lwp.excel.test;

import com.lwp.excel.annotation.*;
import com.lwp.excel.resolver.ExcelAble;
import com.lwp.excel.style.*;
import lombok.Data;
import org.apache.poi.hssf.usermodel.HSSFCell;
import org.apache.poi.hssf.usermodel.HSSFRow;
import org.apache.poi.hssf.usermodel.HSSFSheet;
import org.apache.poi.hssf.usermodel.HSSFWorkbook;
import org.apache.poi.ss.usermodel.CellStyle;
import org.apache.poi.ss.usermodel.Workbook;

import java.util.Date;
import java.util.List;

/**
 * Copyright (C) @2019 GuangDong Eshore Technology Co. Ltd
 *
 * @author: Administrator
 * @version: 1.0
 * @date: 2019/8/9
 * @time: 12:05
 * @description:
 */
@Data
@Sheet(name = "吹雪恒集团")
@Title(value = "吹雪恒集团报表统计",heightInPoints = 30)
public class Shop implements ExcelAble {
    //@Style(border = BorderStyles.BORDER_DASH_DOT,color = ExcelColors.AQUA)
    //@Font(fontHeightInPoints = 12,fontColor = ExcelColors.RED ,fontName = "华文琥珀")
    //@Cell(value = "店名", groups = {Clerk.GroupB.class, Clerk.GroupA.class})
    @Cell(value = "店名")
    private String name;
    //@Style(backgroundColor = ExcelColors.YELLOW,fillPattern = FillPatternStyles.THIN_HORZ_BANDS)
    //@Font(fontHeightInPoints = 14,fontColor = ExcelColors.BLUE , fontName = "Bradley Hand ITC")
    //@Cell(value = "收入",groups = {Clerk.GroupB.class, Clerk.GroupA.class})
    @Cell(value = "收入")
    private Integer income;

    //@Cell(value = "店长",groups = Clerk.GroupA.class)
    @Cell(value = "店长")
    private Manager manager;

    //@Cell(value = "员工", groups = Clerk.GroupB.class)
    @Cell(value = "员工")
    private List<Clerk> clerks ;
    //@Style(valign = ValignStyles.VERTICAL_TOP,halign = HalignStyles.ALIGN_RIGHT)//垂直靠上，水平右对齐
    @Cell(value = "创建时间",format = "yyyy-MM-dd HH:mm:ss")
    private Date createDate;

    @Cell(value = "店铺类型",readConverterExp = "1=超市,2=商场,3=餐饮" )
    private Integer shopType;


    public Shop() {
    }

    public Shop(String name, Integer income) {
        this.name = name;
        this.income = income;
    }

}
