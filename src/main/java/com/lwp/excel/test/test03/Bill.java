package com.lwp.excel.test.test03;

import com.lwp.excel.annotation.Cell;
import com.lwp.excel.annotation.Sheet;
import com.lwp.excel.annotation.Style;
import com.lwp.excel.annotation.Title;
import com.lwp.excel.resolver.ExcelAble;
import lombok.Data;

import java.io.Serializable;
import java.math.BigDecimal;
import java.util.Date;

@Sheet(name = "账单")
@Title(value = "账单报表")
@Data
public class Bill implements Serializable , ExcelAble {
	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;

	/**
	 * 主键
	 */
	@Cell(value = "账单ID")
	private Long id;



	@Cell(value = "App用户")
	private AppUser appUser;

	/**
	 * App用户id
	 */
	//@Cell(value = "收款用户ID")
	private Long appUserId;

	/**
	 * 订单ID
	 */
	@Cell(value = "订单编号")
	private String orderId;

	/**
	 * order_type=1-支付宝账号；order_type=2-微信账号；order_type=3-云闪付账号；order_type=4-农信账号
	 */
	@Cell(value = "收款账号")
	private String account;

	/**
	 * 用户昵称
	 */
	@Cell(value = "收款用户名称")
	private String nickName;

	/**
	 * 平台：0-本地，1-外地 （保留）
	 */

	private Integer platform;

	/**
	 * 客户端定义订单ID
	 */
	private String clientOrderId;

	@Cell(value = "商户")
	private SysUser sysUser;

	/**
	 * 商户定义订单号
	 */
	@Cell(value = "第三方订单号")
	private String recordNo;

	/**
	 * 商户定义唯一编号
	 */
	@Cell(value = "第三方唯一编号")
	private String merchantDefinition;

	/**
	 * 订单金额
	 */
	@Cell(value = "订单金额")
	private BigDecimal orderPrice;

	/**
	 * 订单备注
	 */
	private String orderRemark;

	/**
	 * 随机备注
	 */
	@Cell(value = "订单备注")
	private String randomRemark;

	/**
	 * 订单支付方式(1-支付宝；2-微信；3-银联云闪付；4-农信)
	 */
	@Cell(value = "订单支付方式",readConverterExp = "1=支付宝,2=微信,3=银联云闪付,4=农信")
	private Integer orderType;

	/**
	 * 支付流水号
	 */
	@Cell(value = "流水号")
	private String tradeNo;


	/**
	 * 创建时间
	 */
	@Cell(value = "创建时间",format = "yyyy-MM-dd HH:mm:ss")
	private Date createdAt;

	/**
	 * 更新时间
	 */
	@Cell(value = "更新时间",format = "yyyy-MM-dd HH:mm:ss")
	private Date updatedAt;

	/**
	 * 到期时间(创建时间后4分钟)
	 */
	@Cell(value = "到期时间",format = "yyyy-MM-dd HH:mm:ss")
	private Date expireAt;

	/**
	 * 订单状态(0-未支付；1-已支付；2-已到账；3-已过期)
	 */
	@Cell(value = "订单状态",readConverterExp = "0=未支付,1=已支付,2=已到账,3=已过期")
	private Integer orderState;

	/**
	 * 回调状态(0-未回调；1-回调成功；2-回调失败)
	 */
	@Cell(value = "回调状态",readConverterExp = "0=未回调,1=回调成功,2=回调失败")
	private Integer callbackState;

	/**
	 * 商户秘钥
	 */
	private String secretKey;

	/**
	 * 异常标记（0-需要验证的是否异常；1-不需验证是否异常）
	 */
	private Integer abnormalSign;


	/**
	 * 付款二维码地址
	 */
	private String payCode;

	/**
	 * 是否补单(0-补单；1-非补单)
	 */
	private Integer isReplacement;




}