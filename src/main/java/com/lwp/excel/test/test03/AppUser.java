package com.lwp.excel.test.test03;

import com.lwp.excel.annotation.Cell;
import com.lwp.excel.annotation.Style;
import com.lwp.excel.resolver.ExcelAble;
import lombok.Data;

import java.io.Serializable;
import java.math.BigDecimal;

@Data
public class AppUser implements Serializable , ExcelAble {
	/**
	* 
	*/
	private static final long serialVersionUID = 1L;

	/**
	 * 主键
	 */
	@Cell(value = "收款用户ID")
	private Long id;

	/**
	 * 绑定第三方账号ID
	 */
	private Long thirdPartyId;

	/**
	 * 账号
	 */
	@Cell(value = "App用户账号")
	private String account;

	/**
	 * 密码
	 */
	private String password;

	/**
	 * app用户余额
	 */
	private BigDecimal balance;

	/**
	 * 每日限制收账金额
	 */
	private BigDecimal maxTransferAmount;

	/**
	 * 收账状态(0-开启;1-关闭)
	 */
	private Short collectState;

	/**
	 * 账户异常状态(0-正常;1-异常)
	 */
	private Short abnormalState;

	/**
	 * 账户系统状态(0-正常;1-冻结)
	 */
	private Short systemState;

	/**
	 * 用户websocket在线状态(0-在线;1-离线)
	 */
	private Short onlineState;

	/**
	 * 用户最后一次登录ip
	 */
	private String ipAddress;

	/**
	 * 创建时间
	 */
	private String createAt;
	

}
