package com.lwp.excel.resolver.impl;

import com.lwp.excel.annotation.Cell;
import com.lwp.excel.exception.CellSetException;
import com.lwp.excel.resolver.AnnotationResolver;

import java.lang.annotation.Annotation;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * Copyright (C) @2019 GuangDong Eshore Technology Co. Ltd
 *
 * @author: Administrator
 * @version: 1.0
 * @date: 2019/8/21
 * @time: 14:28
 * @description:
 */
public class CellAnnotationResolver <T extends Annotation> implements AnnotationResolver<T> {


    public final String READ_CONVERTER_EXP = "readConverterExp";

    /**
     * 解析Cell注解，获取替换字符串。
     *
     * @param annotation
     * @param target
     * @return
     */
    @Override
    public Object resolve(T annotation, Object target){
        if (target.equals(READ_CONVERTER_EXP)) {//解析readConverterExp属性
            return resolve(((Cell) annotation).readConverterExp());
        }
        return null;
    }

    public Map<String,Object> resolve(String readConverterExp){
        if (readConverterExp == null || readConverterExp.equals("")) {
            return null;
        }
        Map<String, Object> converter = new HashMap<>();
        String [] readConverters = readConverterExp.split(",");
        for (String readCon :
                readConverters) {
            String[] conStrs = readCon.split("=");
            if (conStrs == null || conStrs.length != 2) {//不符合模板规范
                throw new CellSetException("Incorrect cell setting:Error setting \""+readConverterExp+"\" for Cell property readConverterExp");
            }
            converter.put(conStrs[0], conStrs[1]);
        }
        return converter;
    }


}
