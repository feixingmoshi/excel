package com.lwp.excel.resolver;

import org.apache.poi.hssf.usermodel.HSSFCell;
import org.apache.poi.hssf.usermodel.HSSFRow;
import org.apache.poi.hssf.usermodel.HSSFSheet;

import java.lang.reflect.Field;
import java.util.List;

public interface ExcelResolver {


    /**
     * 获取最后一行的索引
     * @param sheet
     * @return
     */
    public int lastRowIndex(HSSFSheet sheet);


    /**
     * 获取
     * @param row
     * @return
     */
    public int lastCellIndex(HSSFRow row);


    /**
     * 获取数据最大粒子行数：
     * 传入一条数据，
     * 返回一个装在该数据需要的行数。
     *
     * @param clazz
     * @return 传入数据需要多少行能够展示该数据。
     */
    public int countParticleRow(Class<?> clazz,String[] headers);


    /**
     * 传入一条数据，
     * 返回一个装在该数据需要的列数。
     * @param clazz
     * @return
     */
    public int countParticleCell(Class<?>  clazz,String[] headers);


    /**
     * 传入一个数据 ，
     * 获取该属性需要多少行来装载每条数据
     * 分两种情况：
     *  1.字段数据类型为ExcelAble 或其他基本类型及包装类型。这样装载每条数据需要1行。
     *  2.字段是集合类型，那么是按照集合列表及子集合列表的数据条数和。
     * @param data
     * @return
     */
    public int countParticleValRow(Object  data,String[] headers);








}
